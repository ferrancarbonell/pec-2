<h1>PEC2 - Jungle Cowboy</h1>
<p>Ésta práctica consiste en el diseño e implementación del primer nivel 1-1 de Super Mario BROS para NES, pero con toda la parte gráfica nueva. En éste caso se ha utilizado un Tileset de la jungla con dinosaurios como enemigos y un cowboy como protagonista.</p>

<h2>Implementación y estructuración</h2>
<p>Éste juego se ha desarollado con Unity en la versión 2018.2.14.</p>

<p>Los assets del proyecto se dividen en:</p>
<h3>Scenes</h3>
<p>Se estructura en 4 escenas:</p>
<ul>
<li><strong>Start:</strong> escena inicial para cargar una nueva partida o salir del juego.</li>
<li><strong>Game:</strong> escena de juego donde hay que superar el nivel a través del movimiento del personaje.</li>
<li><strong>Winner:</strong> escena final con los melocotones recogidos y tiempo transcurrido en superar el nivel. También se puede volver a jugar o salir del juego.</li>
<li><strong>Game Over:</strong> escena que aparece cuando el personaje muere y se puede volver a jugar o salir del juego</li>
</ul>

<h3>Audio</h3>
<p>Todos los archivos de audio utilizados entre otros de pruevas. algunos son de creación propia y otros de recursos externos.</p>

<h3>Fonts</h3>
<p>Única fuente tipográfica utilizada</p>

<h3>Scripts</h3>
<ul>
<li><strong>Box Controller: </strong> define el comportamiento de las cajas.</li>
<li><strong>Camera move: </strong> mueve la cámara hacia a la derecha de forma perpetua en todas las escenas excepto la de juego.</li>
<li><strong>Enemy Controller: </strong> define el comportamiento de los enemigos.</li>
<li><strong>Game Options: </strong> define las opciones de los botones de la interfície de juego.</li>
<li><strong>Gameplay Manager: </strong> define el comportamiento entre escenas.</li>
<li><strong>Hero Controller: </strong> define el comportamiento del héro que controla el jugador.</li>
<li><strong>Hero Follower: </strong> pone el target a seguir de la cámara principal en la escena de juego.</li>
<li><strong>Powerup: </strong> elimina el powerup cuando el jugador lo recoge.</li>
<li><strong>Scrolling Background: </strong> define un efecto parallax entre los elementos de la escena, el background y el foreground.</li>
<li><strong>Winner: </strong> carga los datos de melocotones y tiempo en la pantalla final una vez superado el nivel.</li>
</ul>

<h3>Prefabs</h3>
<p>Donde se encuentran los prefabs de enemigo, cajas y alguns elementso de decoración.</p>

<h3>Sprites</h3>
<p>Las imágenes, tilemaps y paletas de sprites del juego organizados en: </p>
<ul>
<li>Background</li>
<li>Boc</li>
<li>Enemy</li>
<li>Hero</li>
<li>Objects</li>
<li>Sprites Palettes</li>
<li>Tilemaps</li>
</ul>

<h3>Animations</h3>
<p>Todas las animaciones del juego organizadas por:</p>
<ul>
<li>Hero</li>
<li>Enemy</li>
<li>Box</li>
<li>Camera</li>
<li>Treasue</li>
</ul>

<h3>Materials</h3>
<p>Donde se situa el único material del juego para que el personaje rebote en los enemigos.</p>

<h2>Recursos externos</h2>
<ul>
<li><strong>Dinosaur Park </strong>(GameArt2D.com) https://www.gameart2d.com/dinosaur-park-game-sprites.html</li>
<li><strong>Fantasy Sfx </strong>(LITTLE ROBOT SOUND FACTORY) https://assetstore.unity.com/packages/audio/sound-fx/fantasy-sfx-32833a</li>
<li><strong>Tribal Jungle Music Free Pack </strong>(TYLER CUNNINGHAM) https://assetstore.unity.com/packages/audio/music/tribal-jungle-music-free-pack-131414</li>
<li><strong>Cartoon Game Sound </strong>(J.BOB SOUND STUDIO) https://assetstore.unity.com/packages/audio/sound-fx/cartoon-game-sound-5438</li>
<li><strong>AlfaSlabOne-Regular </strong>(JM Solé) https://fonts.google.com/specimen/Alfa+Slab+One</li>
</ul>

<h2>Como jugar</h2>
<p>Para jugar en la pantalla principal hay que clicar a "nueva partida". El objetivo principal es recoger el máximo de melocotones y completar el nivel el más rápido posible.</p>
<p>El personaje se controla con las flechas horizontales y el espacio para saltar.</p>
<p>En el nivel hay que sortear obstáculos y enemigos que se pueden eliminar saltando sobre ellos. También existen uns bloques que se pueden activar para conseguir melocotones o powerups. Al final se encuentra la llave que abrirá el cofre y finalizará la partida.</p>

<p>link del gameplay: https://vimeo.com/306675799</p>
<h2>Bugs Conocidos</h2>
<p>Alguna vez el personaje se queda parado indefinidamente y otras tiene que saltar para avanzar en el suelo.