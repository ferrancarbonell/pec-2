﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
	public Text peachCounterText;
	public Text timeText;
	
	public static int peachCounter;
	public static string finishTime;
	
	float startTime;


	void Start()
	{
		// Inicializamos el contador de melocotones y guardamos el valor del tiempo inicial
		peachCounter = 0;
		startTime = Time.time;
	}

	void Update ()
	{
		// Actualizamos el contador de melocotones y el contador del tiempo transcurrido
		peachCounterText.text = "" + peachCounter.ToString();
		timeText.text = (Time.time - startTime).ToString("00") + "s";

		// Si el jugador completa el nivel guardamos el tiempo que ha tardado en completarlo
		if (HeroController.finish)
		{
			finishTime = (Time.time - startTime).ToString("00") + "s";
			SceneManager.LoadScene("Winner");
		}

		// Si el jugador está muerto cargamos la escena de Game Over
		if (HeroController.gameOver)
			SceneManager.LoadScene("Game Over");	
	}
}
