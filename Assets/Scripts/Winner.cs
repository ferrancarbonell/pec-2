﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Winner : MonoBehaviour
{
	public Text timeFinish;
	public Text peachCount;
	void Start ()
	{
		// Cargamos los datos guardados de melocotones y tiempo en completar el nivel
		peachCount.text = GameplayManager.peachCounter.ToString();
		timeFinish.text = GameplayManager.finishTime;
	}
	
}
