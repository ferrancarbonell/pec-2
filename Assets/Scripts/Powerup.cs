﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour {

	// Si es colisionado por el jugador se elimina
	void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Hero")
		{
			Destroy(gameObject);
		}
	}
}
