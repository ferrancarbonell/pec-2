﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour 
{
	public enum BoxType {surprise,wood,powerup,used}
	public BoxType boxType;
	public Sprite surpriseSprite;
	public Sprite woodSprite;
	public Sprite usedSprite;
	public RuntimeAnimatorController explosion;
	public AudioClip audioBoxCrash;
	public AudioClip audioBoxSurprise;
	public AudioClip audioBoxPowerup;
	SpriteRenderer spriteRenderer;
	Animator anim;
	AudioSource audioSource;

	void Start ()
	{	
		// Cargamos los valores de los componentes
		anim = gameObject.GetComponentInParent<Animator>();
		audioSource = GetComponent<AudioSource>();
		// Dependiedo del tipo de caja carga una sprite inicial u otra
		spriteRenderer = gameObject.GetComponentInParent<SpriteRenderer>();
		if (boxType == BoxType.surprise || boxType == BoxType.powerup)
			spriteRenderer.sprite = surpriseSprite;
		if (boxType == BoxType.wood)
			spriteRenderer.sprite = woodSprite;
	}

	// Cuando el estado de la caja sea usada cambia el sprite
	void Update()
	{
		if (boxType == BoxType.used)
			spriteRenderer.sprite = usedSprite;
	}
	// Muestra como actua la caja en ser colisionada por el jugador
	void OnTriggerEnter2D(Collider2D collision)
    {
			// Si la caja es de sorpresa activa la animación del melocotón
			if (boxType == BoxType.surprise)
			{
				gameObject.transform.GetChild(0).gameObject.SetActive(true);
				audioSource.PlayOneShot(audioBoxSurprise, 0.5f);
				boxType = BoxType.used;
				GameplayManager.peachCounter ++;
			}
			// Si la caja es de powerup activa la animación del powerup
			if (boxType == BoxType.powerup)
			{
				gameObject.transform.GetChild(0).gameObject.SetActive(true);
				audioSource.PlayOneShot(audioBoxPowerup, 0.5f);
				boxType = BoxType.used;
			}
			// Si la caja es de madera se puede romper si el jugador está en estado powerup
			if (boxType == BoxType.wood && HeroController.heroState == HeroController.HeroState.powerup)
			{
				anim.runtimeAnimatorController = explosion;
				anim.enabled = true;
				audioSource.PlayOneShot(audioBoxCrash, 0.3f);
				Destroy(transform.parent.gameObject, 0.9f);
			}
    }
}
