﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
	
	void Update () 
	{
		// Mueve la cámara de forma perpetua hacia la derecha
		transform.Translate(Vector2.right * Time.deltaTime, Camera.main.transform);
	}
}
