﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class EnemyController : MonoBehaviour 
{	
	public float maxSpeed = 10f;
	public float move = 0.2f;
	public AudioClip audioKiller;
	public AudioClip audioAttack;

	bool facingRight = true;
	enum EnemyState {alive, death, attack}
	EnemyState enemyState;
    Animator anim;
	Rigidbody2D myRigidbody2D;
	SpriteRenderer spriteRenderer;
	AudioSource audioSource;

	void Start ()
	{
		// Asignamos el estado inicial del enemigo
		enemyState = EnemyState.alive;
		// Cargamos los valores de los componentes
		myRigidbody2D = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		audioSource = GetComponent<AudioSource>();
	}	
	
	void FixedUpdate() 
	{
		// Si el enemigo está vivo se mueve
        if (enemyState == EnemyState.alive)
		{
			myRigidbody2D.velocity = new Vector2 (move * maxSpeed, myRigidbody2D.velocity.y);

			if (move > 0 &&!facingRight)
        		Flip();
        	else if (move < 0 && facingRight)
        		Flip();
		}
		// Si el enemigo está muerto activa la animación de morir y se destruye al cavo de poco
		if (enemyState == EnemyState.death)
		{
			anim.SetBool("Death", true);
			Destroy(gameObject, 0.8f);
		}
		// Si el enemigo está en estado de ataque se activa la animación de ataque
		// y vuelve al estado inicial
		if (enemyState == EnemyState.attack)
		{
			anim.SetTrigger("Attack");
			enemyState = EnemyState.alive;
		}
	}
	// Función para girar la imagen del enemigo
	void Flip()
	{
  		facingRight = !facingRight;
        spriteRenderer.flipX = !facingRight; 
	}
	// Cuando detecta una colision de los pies del jugador se muere
	void OnTriggerEnter2D(Collider2D collision)
    {
			if (collision.gameObject.tag == "Killer")
			{	
				audioSource.PlayOneShot(audioKiller, 1);
				enemyState = EnemyState.death;
			}	
    }
	// Cuando colisiona con diferentes elementos actua de varias formas
	void OnCollisionEnter2D(Collision2D other)
    {
		// Cambia de sentido si colisiona con todo excepto el suelo y el jugador
		if (other.gameObject.tag != "Tilemap" && other.gameObject.tag != "Hero")
			move *= -1;
		// Si cae en la zona de muerte de elimina
		if (other.gameObject.tag == "DeathZone")
			Destroy(gameObject);
		// Si colisiona con el jugador lo muerde
		if (other.gameObject.tag == "Hero")
		{
			audioSource.PlayOneShot(audioAttack, 1);
			enemyState = EnemyState.attack;
		}
    }
}


